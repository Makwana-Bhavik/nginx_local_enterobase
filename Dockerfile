FROM nginx:1.15.8

COPY nginx.conf /var/www/nginx.conf
COPY certs/ var/www/certs
COPY nginx.conf /home/nginx_user/nginx.conf
COPY certs/ /home/nginx_user/certs
#COPY logs/ /home/nginx_user/logs/
CMD ["chmod", "700", "/home/nginx_user/certs"]
CMD ["nginx", "-g", "daemon off;", "-c", "/home/nginx_user/nginx.conf"]

